from django import forms

class StatusForm(forms.Form):
    attrs = {
        'class' : 'form-control',
        'placeholder': "Masukkan statusmu...",
        'id': 'content-text'
    }

    error_messages = {
        'required': 'Tolong isi input ini!',
    }

    status = forms.CharField(label='Status', max_length=300, widget=forms.TextInput(attrs=attrs))

class SubscribeForm(forms.Form):
    name = forms.CharField(label='Name', max_length=100,
                           widget=forms.TextInput(attrs={'id': 'name', 'class': 'form-control', 'placeholder': 'Enter your name'}))
    email = forms.EmailField(label='Email', max_length=100,
                             widget=forms.EmailInput(attrs={'id': 'email', 'class': 'form-control', 'placeholder': 'Enter your email'}))
    password = forms.CharField(label='Password', max_length=30, widget=forms.PasswordInput(
                               attrs={'id': 'password', 'class': 'form-control', 'placeholder': 'Enter your password'}))

    