from django.db import IntegrityError
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import landingpage, post_message, profile, books, tableofBooks, booksSearch, subscribe, validate_email, post_subscribe, get_data_subscribe, result_subscribe, delete_from_model, logout, login
from .models import StatusModel, SubscribeModel
from .forms import StatusForm, SubscribeForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

# Create your tests here.


class Lab6UnitTest(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/websiteBaruku/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_landingpage_func(self):
        found = resolve('/websiteBaruku/')
        self.assertEqual(found.func, landingpage)

    def test_model_can_add_new_status(self):
        status_message = StatusModel.objects.create(status='Halo')
        counting_all_status_message = StatusModel.objects.all().count()
        self.assertEqual(counting_all_status_message, 1)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'][0],
            'This field is required.',
        )

    def test_lab6_using_method_post(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['statusku'] = "Hello"
        post_message(request)

    def test_lab6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/post_message/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/post_message/', {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_lab6_url_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_lab6_url_books_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_books_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)

    # def test_lab6_url_tableofBooks_is_exist(self):
    #     response = Client().get('/tableofBooks/')
    #     self.assertEqual(response.status_code, 200)

    def test_lab6_using_tableofBooks_func(self):
        found = resolve('/tableofBooks/')
        self.assertEqual(found.func, tableofBooks)

    def test_lab6_url_bookSearch_is_exist(self):
        response = Client().get('/search/ha')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_bookSearch_func(self):
        found = resolve('/search/ha')
        self.assertEqual(found.func, booksSearch)

    def test_url_subscribe_page_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_url_subscribe_using_subscribe_function(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_url_validate_page_is_exist(self):
        response = Client().post('/validate/', data={
            "email": "emailku@gmail.com"
        })
        self.assertEqual(response.status_code, 200)

    def test_url_validatee_using_subscribe_function(self):
        found = resolve('/validate/')
        self.assertEqual(found.func, validate_email)

    def test_subscribe_model_can_save_data(self):
        SubscribeModel.objects.create(
            name="Tester", email='test@ui.ac.id', password="asdasd123")
        counting_all_data = SubscribeModel.objects.all().count()
        self.assertEqual(counting_all_data, 1)

    def test_subscribe_model_func_name(self):
        SubscribeModel.objects.create(
            name='Test', email='a@a.com', password='123')
        subscriber = SubscribeModel.objects.get(email='a@a.com')
        self.assertEqual('a@a.com', subscriber.email)

    def test_subscribe_model_unique_email(self):
        SubscribeModel.objects.create(email="test@email.com")
        with self.assertRaises(IntegrityError):
            SubscribeModel.objects.create(email="test@email.com")

    def test_subscribe_model_using_ajax(self):
        response = Client().post('/post_subscribe/', data={
            "name": "tes",
            "email": "tes@gmail.com",
            "password": "123",
        })
        self.assertEqual(response.status_code, 200)

    def test_url_result_page_is_exist(self):
        response = Client().get('/result/')
        self.assertEqual(response.status_code, 200)

    def test_url_result_using_result_subscribe_function(self):
        found = resolve('/result/')
        self.assertEqual(found.func, result_subscribe)

    def test_url_getdatasubscribe_page_is_exist(self):
        response = Client().get('/getdatasubscribe/')
        self.assertEqual(response.status_code, 200)

    def test_url_getdatasubscribe_using_get_data_subscribe_function(self):
        found = resolve('/getdatasubscribe/')
        self.assertEqual(found.func, get_data_subscribe)

    def test_url_delete_page_is_exist(self):
        response = Client().post('/delete/', data={
            "email": "emailku@gmail.com"
        })
        self.assertEqual(response.status_code, 200)

    def test_url_delete_using_delete_from_model_function(self):
        found = resolve('/delete/')
        self.assertEqual(found.func, delete_from_model)

    def test_url_login_page_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_login_using_login_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login)

    def test_url_logout_page_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_url_logout_using_logout_function(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logout)

    def test_lab11_false_token(self):
        response = Client().post('/login/', {'id_token': "testtttt"}).json()
        self.assertEqual(response['status'], "1")

    def test_lab11_right_token(self):
        response = Client().post('/login/', {
            'id_token': "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ2M2ZlNDgwYzNjNTgzOWJiYjE1ODYxZTA4YzMyZDE4N2ZhZjlhNTYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMjc1OTI1OTQxNDktc2YzMGcwZHFoY3ZqM2dqbzg3cmg5aHNuY2RsNmhkZXIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIyNzU5MjU5NDE0OS1zZjMwZzBkcWhjdmozZ2pvODdyaDloc25jZGw2aGRlci5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsInN1YiI6IjEwMTg3MjY0MTg2NzQ2MzM0MDQ0MSIsImVtYWlsIjoidGhlcmVzaWFnbmVzaGFuZG9rb0BnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IkF1akxOR3Y1ak5SSlRUc2hQRWVKc1EiLCJuYW1lIjoiQWduZXMgSGFuZG9rbyIsInBpY3R1cmUiOiJodHRwczovL2xoNC5nb29nbGV1c2VyY29udGVudC5jb20vLWROMURVR2d2RzNNL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FHRGd3LWdhZGZFeUtWWXhBYjVlSVM1bnpJcVpjNGRiU3cvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IkFnbmVzIiwiZmFtaWx5X25hbWUiOiJIYW5kb2tvIiwibG9jYWxlIjoiZW4iLCJpYXQiOjE1NDM1ODI1MTMsImV4cCI6MTU0MzU4NjExMywianRpIjoiMTE0NDA0YWQ5YjExYjA0YzZkNTBjMGYzYjcwNDJiNDMwMjg1MDExMSJ9.QSzlMrQan3-atFprgsF7eMeQdrs96jWUosEDeG8RuPIKdiRkJXbnd7UK1fwz0dmgR5r3YBvCk1PTKA_bpzZhZiDvWWx4XE-RJoHXALShW60hONAOrlvsg-VluJR_BG5DVN_7Vp_gEHsZqzj5Ft4sKU7gSMEw3EIKf1e5C4FyhBBJwMrgCLak-Ne62snG_CGaWCQEMnOOGjTp8u9L3Exs_DaBaW9vQ8RTPGnlkbLRttII6s88HTmN-9gCJ0jduhVAq9yE0GJZXSWZ41ELdr8ASKuNwOlIHwd6Fk_uoK0t7y6QWu6v-mie_l_Dvz_Hg4CRYGh7EsWxwiocE9aXNxkzTA"}).json()
        self.assertEqual(response['status'], "1")

    def test_logout(self):
        response = Client().get('/login/logout/')
        self.assertEqual(response.status_code, 200)


class NewModelTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(NewModelTest, self).setUp()

    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()
        super(NewModelTest, self).tearDown()

    def test_can_start_a_model_and_retrieve_it_later(self):
        # Students want to check their Lab-6 models works
        self.browser.get('http://status-agnes.herokuapp.com/')
        time.sleep(5)  # Let the user actually see something!
        input_box = self.browser.find_element_by_name('status')
        input_box.send_keys('LinkedList susah bgt')

        submit_btn = self.browser.find_element_by_id('button')
        submit_btn.submit()
        time.sleep(5)  # Let the user actually see something!
        self.assertIn("LinkedList", self.browser.page_source)
        time.sleep(5)  # Let the user actually see something!

    def test_header_top_text_layout(self):
        self.browser.get('http://status-agnes.herokuapp.com/')
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn("Halo, apa kabar?", header_text)

    # def test_header_after_top_text_layout(self):
    #     self.browser.get('http://status-agnes.herokuapp.com/')
    #     header_text = self.browser.find_element_by_tag_name('article').text
    #     self.assertIn("Status", header_text)

    def test_definition_text_color_is_blue_css(self):
        self.browser.get('http://status-agnes.herokuapp.com/')
        color = self.browser.find_element_by_class_name(
            'definition_text').value_of_css_property('color')
        self.assertIn("rgba(25, 25, 112, 1)", color)

    def test_definition_text_font_is_FiraSans_css(self):
        self.browser.get('http://status-agnes.herokuapp.com/')
        font = self.browser.find_element_by_class_name(
            'definition_text').value_of_css_property('font-family')
        self.assertIn("Fira Sans", font)
