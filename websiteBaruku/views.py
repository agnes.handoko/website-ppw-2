from django.shortcuts import render
from google.oauth2 import id_token
from google.auth.transport import requests as google_request
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.urls import reverse
from .forms import StatusForm, SubscribeForm
from .models import StatusModel, SubscribeModel
import json
import requests

response = {'author': 'Agnes'}

# Create your views here.


def profile(request):
    return render(request, 'profile.html')


def landingpage(request):
    status = StatusModel.objects.all()
    response['statusku'] = status
    response['statusForm'] = StatusForm
    html = 'landingpage.html'
    return render(request, html, response)


def post_message(request):
    form = StatusForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['statusku'] = request.POST['status']
        message = StatusModel(status=response['statusku'])
        message.save()
        html = 'landingpage.html'
    return HttpResponseRedirect('/websiteBaruku/')


def books(request):
    jsonFile = requests.get(
        "https://www.googleapis.com/books/v1/volumes?q=quilting").json()
    listofBooks = []
    for items in jsonFile['items']:
        newdict = {"image": items['volumeInfo']['imageLinks']
                   ['thumbnail'], "title": items['volumeInfo']['title']}
        listofBooks.append(newdict)
    return JsonResponse({'data': listofBooks})


def booksSearch(request, param):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + param
    jsonFile = requests.get(url).json()
    listofBooks = []
    for items in jsonFile['items']:
        newdict = {"image": items['volumeInfo']['imageLinks']
                   ['thumbnail'], "title": items['volumeInfo']['title']}
        listofBooks.append(newdict)
    return JsonResponse({'data': listofBooks})


def tableofBooks(request):
    response['name'] = request.session['name']
    return render(request, 'books.html', response)


def subscribe(request):
    # subscribe = SubscribeModel.objects.all()
    response['subs'] = subscribe
    response['subscribeForm'] = SubscribeForm
    html = 'subscribe.html'
    return render(request, html, response)


def validate_email(request):
    if request.method == 'POST':
        email = request.POST['email']
        check_email = SubscribeModel.objects.filter(email=email)
        if check_email.exists():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})


def post_subscribe(request):
    form = SubscribeForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        print('halo')
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        SubscribeModel.objects.create(
            name=response['name'], email=response['email'], password=response['password'])
        return JsonResponse({'is_success': True})


def get_data_subscribe(request):
    # if request.method == 'POST':
    dataSubscribe = SubscribeModel.objects.all().values()
    listDataSubscribe = list(dataSubscribe)
    return JsonResponse({'listDataSubscribe': listDataSubscribe})


def result_subscribe(request):
    return render(request, 'SubscriberTabel.html')


def delete_from_model(request):
    if request.method == 'POST':
        email = request.POST['email']
        SubscribeModel.objects.filter(email=email).delete()
        now = list(SubscribeModel.objects.all().values())
        return JsonResponse({'deleted': True, 'now': now})


def login(request):
    if request.method == 'POST':
        try:
            token = request.POST['id_token']
            # print(token)
            id_info = id_token.verify_oauth2_token(token, google_request.Request(
            ), '27592594149-sf30g0dqhcvj3gjo87rh9hsncdl6hder.apps.googleusercontent.com')
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')

            user_id = id_info['sub']
            name = id_info['name']
            email = id_info['email']
            request.session['user_id'] = user_id
            request.session['name'] = name
            request.session['email'] = email
            request.session['books'] = []

            return JsonResponse({'status': '0', 'url': reverse('tableofBooks')})
        except ValueError:
            return JsonResponse({'status': '1'})
    return render(request, 'login.html')


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))
