from django.conf.urls import url
from django.urls import path
from .views import landingpage, post_message, profile, books, tableofBooks, booksSearch, subscribe, post_subscribe, validate_email, get_data_subscribe, result_subscribe, delete_from_model, login, logout

urlpatterns = [
    url('login/', login, name='login'),
    url('logout', logout, name='logout'),
    url('post_subscribe', post_subscribe, name='post_subscribe'),
    url('validate', validate_email, name='validate'),
    url('getdatasubscribe', get_data_subscribe, name="getdatasubscribe"),
    url('result', result_subscribe, name="result"),
    url('delete', delete_from_model, name='delete'),
    url('profile', profile),
    url('landingpage', landingpage),
    url('post_message', post_message, name='post_message'),
    url('subscribe', subscribe),
    url('books', books),
    path('search/<str:param>', booksSearch),
    url('tableofBooks', tableofBooks, name='tableofBooks'),
    url('', landingpage)
]
