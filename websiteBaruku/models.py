from django.db import models

class StatusModel(models.Model):
    status = models.CharField(max_length=30)
    created_date = models.DateTimeField(auto_now_add=True)

class SubscribeModel(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, unique=True)
    password = models.CharField(max_length=30)