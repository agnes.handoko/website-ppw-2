$(document).ready(function () {
    gapi.load('auth2', function () {
        // gapi.auth2.init({
        //     client_id: '27592594149-sf30g0dqhcvj3gjo87rh9hsncdl6hder.apps.googleusercontent.com'
        // });
        gapi.auth2.init();
    });
});

function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    console.log(id_token);
    sendToken(id_token);
    console.log("Logged in");
};

function signOut() {
    gapi.load('auth2', function () {
        // gapi.auth2.init({
        //     client_id: '27592594149-sf30g0dqhcvj3gjo87rh9hsncdl6hder.apps.googleusercontent.com'
        // });
        gapi.auth2.init().then(() => {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
            });
        })
    });

};

function sendToken(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;
    console.log("masuk gasih");
    $.ajax({
        method: 'POST',
        url: "/login/",
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {
            id_token: token
        },
        success: function (result) {
            console.log("Successfully send token");
            if (result.status === "0") {
                html = "<h4>Logged In</h4>";
                window.location.replace(result.url);
            } else {
                html = "<h4>Something error, please report</h4>"
            }
            $("h4").replaceWith(html)
        },
        error: function () {
            alert("Something error, please report")
        }
    })
};