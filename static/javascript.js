$(document).ready(function() {
    $(window).on('load', function () {
        $('#status').delay(800).fadeOut();
        $('#preloader').delay(1000).fadeOut('slow');
    });

    $("#accordion").accordion({
        collapsible: true,
        active: false
    });

    // VALIDASI FORM
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email_is_available;
    var timer = 0;

    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(checkValidEmailFormat, 1000);
        toggleButton();
    });

    $('#name').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(toggleButton, 1000);
    });

    $('#password').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(toggleButton, 1000);
    });

    $('#form').on('submit', function (event) {
        event.preventDefault();
        console.log("Form Submitted!");
        sendFormData();
    });

    function validateEmail() {
        $.ajax({
            method: 'POST',
            url: "/validate",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                email: $('#email').val(),
            },
            success: function (email) {
                if (email.is_exists) {
                    email_is_available = false;
                    $('.definition_text_red p').replaceWith("<p class='fail'>This email is already been used, please use another email!</p>");
                } else {
                    email_is_available = true;
                    toggleButton();
                }
            },
            error: function () {
                alert("Error, cannot validate email!")
            }
        })
    }

    function sendFormData() {
        $.ajax({
            method: 'POST',
            url: "/post_subscribe",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                name: $('#name').val(),
                email: $('#email').val(),
                password: $('#password').val(),
            },
            success: function (response) {
                if (response.is_success) {
                    $('#name').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#submit-btn').prop('disabled', true);
                    $('.definition_text_red p').replaceWith("<p style='color:green' >Data successfully saved!</p>");
                    console.log("Successfully add data");
                } else {
                    $('.definition_text_red p').replaceWith("<p>Error! Data cannot be saved!</p>");
                }
            },
            error: function () {
                alert("Error, cannot save data to database");
            }
        })
    }

    function checkValidEmailFormat() {
        var reg = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var is_valid = reg.test($('#email').val());

        if (is_valid) {
            validateEmail();
        } else {
            $('.definition_text_red p').replaceWith("<p>Please enter a valid email format!</p>");
        }
    }

    function toggleButton() {
        var password = $('#password').val();
        var name = $('#name').val();
        var email = $('#email').val();
        if (password.length !== 0 && name.length !== 0 && email_is_available) {
            $('.definition_text_red p').replaceWith("<p></p>");
            $('#submit-btn').prop('disabled', false);
        } else if (password.length === 0 && name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.definition_text_red p').replaceWith("<p>Name and password cannot be empty</p>");
        } else if (password.length === 0 && email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.definition_text_red p').replaceWith("<p>Email and password cannot be empty</p>");
        } else if (name.length === 0 && email.length) {
            $('#submit-btn').prop('disabled', true);
            $('.definition_text_red p').replaceWith("<p>Name and email cannot be empty</p>");
        } else if (password.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.definition_text_red p').replaceWith("<p>Password cannot be empty</p>");
        } else if (name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.definition_text_red p').replaceWith("<p>Name cannot be empty</p>");
        } else if (email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.definition_text_red p').replaceWith("<p>Email cannot be empty</p>");
        } else {
            $('#submit-btn').prop('disabled', true);
            $('.definition_text_red p').replaceWith("<p>Please enter a valid email format!</p>");
        }
    }
    // AKHIR VALIDASI FORM

    $.ajax({
        method: "GET",
        url: "/books",
        success: function(hasil) {
            var data = hasil.data;
            for (var i = 0; i < data.length; i++) {
                var title = data[i].title;
                var image = data[i].image;
                var imageFinal = '<img' + ' width="80" height="100" src="' + image + '">';
                var id = "bintangku" + i;
                var button = '<img id="' + id + '" onclick="changeImage(id)" width="25" height="25" src="https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png">';
                var html = '<tr>' + '<td>' + imageFinal + '</td>' + '<td>' + title + '</td>' + '<td>' +button+ '</td>' +'</tr>';
                $('#tbody').append(html);
            }
        },
        error: function(error) {
            alert("Halaman buku tidak ditemukan")
        }
    });

    $.ajax({
        // method: 'POST',
        url: '/getdatasubscribe',
        // headers: {
        //     "X-CSRFToken": csrftoken
        // },
        success: function (result) {
            var length_arr = result.listDataSubscribe.length;
            var data = result.listDataSubscribe;
            for (var i = 0; i < length_arr; i++) {
                var name = data[i]['name'];
                var email = data[i]['email'];
                var button = '<button class="btn" id=' + email + ' onclick=unsubscribe(this)>' + 'UNSUBSCRIBE' + '</button>';
                var show = '<tr>' + '<td>' + name + '</td><td>' + email + '</td><td>' + button + '</td>';
                $('#tbodySubscribe').append(show)
            }
        }
    });
    


    document.getElementById('changetheme').onclick = changeTheme;
    function changeTheme() {
        if ($("#page1").hasClass('page1')) {
            $("#page1").removeClass('page1');
            $("#page1").addClass('page1_new');
            $("#text_page1_1").removeClass('text_page1');
            $("#text_page1_1").addClass('text_page1_new');
            $("#text_page1_2").removeClass('text_page1');
            $("#text_page1_2").addClass('text_page1_new');
            $("#text_page1_3").removeClass('text_page1');
            $("#text_page1_3").addClass('text_page1_new');
            $("#definition_text_1").removeClass('definition_text');
            $("#definition_text_1").addClass('definition_text_new');
            $("#definition_text_2").removeClass('definition_text');
            $("#definition_text_2").addClass('definition_text_new');
            $("#background_font2").removeClass('background_font2');
            $("#background_font2").addClass('background_font2_new');
        } else {
            $("#page1").removeClass('page1_new');
            $("#page1").addClass('page1');
            $("#text_page1_1").removeClass('text_page1_new');
            $("#text_page1_1").addClass('text_page1');
            $("#text_page1_2").removeClass('text_page1_new');
            $("#text_page1_2").addClass('text_page1');
            $("#text_page1_3").removeClass('text_page1_new');
            $("#text_page1_3").addClass('text_page1');
            $("#definition_text_1").removeClass('definition_text_new');
            $("#definition_text_1").addClass('definition_text');
            $("#definition_text_2").removeClass('definition_text_new');
            $("#definition_text_2").addClass('definition_text');
            $("#background_font2").removeClass('background_font2_new');
            $("#background_font2").addClass('background_font2');
        }
    }
});

function changeImage(id) {
    $.ajax({
        method: "GET",
        url: "/books",
        success: function(hasil) {
            count = document.getElementById('counter').innerHTML;
            count2 = parseInt(count);
            console.log(count2);
            var data = hasil.data;
            for (var i = 0; i < data.length; i++) {
                var id2 = "bintangku" + i;
                if(id == id2) {
                    var image = document.getElementById(id2);
                    if (image.src.match("https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png")) {
                        image.src = "https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/favorited.png";
                        count2 = count2 + 1;
                    } else {
                        image.src = "https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png";
                        count2 = count2 - 1;
                    }
                }
            }
            $('#counter').replaceWith('<span id="counter">' + count2 + '</span>');
        },
        error: function(error) {
            alert("Halaman buku tidak ditemukan")
        }
    });
};

function search() {
    var valueSearch = document.getElementById("searchBooks").value;
    console.log(valueSearch);
    $('#counter').replaceWith('<span id="counter">' + 0 + '</span>');
    $.ajax({
        method: "GET",
        url: "/search/" + valueSearch,
        success: function(hasil) {
            var data = hasil.data;
            $('#tbody').replaceWith('<tbody id="tbody"></tbody>');
            for (var i = 0; i < data.length; i++) {
                var image = data[i].image;
                var imageFinal = '<img' + ' width="80" height="100" src="' + image + '">';
                var title = data[i].title;
                var id = "bintangku" + i;
                var button = '<img id="' + id + '" onclick="changeImage(id)" width="25" height="25" src="https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png">';
                var html = '<tr>' + '<td>' + imageFinal + '</td>' + '<td>' + title + '</td>' +  '<td>' +button+ '</td>' +'</tr>';
                $('#tbody').append(html);
            }
        },
        error: function(error) {
            alert("Halaman buku tidak ditemukan")
        }
    });
};

function renderResult() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: '/getdatasubscribe',
        headers: {
            "X-CSRFToken": csrftoken
        },
        success: function (result) {
            var length_arr = result.listDataSubscribe.length;
            var data = result.listDataSubscribe;
            for (var i = 0; i < length_arr; i++) {
                var name = data[i]['name'];
                var email = data[i]['email'];
                var button = '<button class="btn" id=' + email + ' onclick=unsubscribe(this)>' + 'UNSUBSCRIBE' + '</button>';
                var show = '<tr>' + '<td>' + name + '</td><td>' + email + '</td><td>' + button + '</td>';
                $('#tbodySubscribe').append(show)
            }
        }
    })
};

function unsubscribe(t) {
    var txt;
    var person = prompt("Please enter your password:", "password");
    if (person == null || person == "") {
        txt = "User cancelled the prompt.";
    } else {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        var email = t.id;
        $.ajax({
            url: '/getdatasubscribe',
            success: function (result) {
                var length_arr = result.listDataSubscribe.length;
                var data = result.listDataSubscribe;
                for (var i = 0; i < length_arr; i++) {
                    if(email ==  data[i]['email']) {
                        if(person == data[i]['password']) {
                            $.ajax({
                                method: 'POST',
                                url: "/delete",
                                headers: {
                                    "X-CSRFToken": csrftoken
                                },
                                data: {email: email},
                                success: function (res) {
                                    console.log('success');
                                    $('#tbodySubscribe').replaceWith("<tbody id='tbodySubscribe'></tbody>");
                    
                                    var length_arr = res.now.length;
                                    var data = res.now;
                                    for (var i = 0; i < length_arr; i++) {
                                        var name = data[i]['name'];
                                        var email = data[i]['email'];
                                        var button = '<button class="btn" id=' + email + ' onclick=unsubscribe(this)>' + 'UNSUBSCRIBE' + '</button>';
                                        var show = '<tr>' + '<td>' + name + '</td><td>' + email + '</td><td>' + button + '</td>';
                                        $('#tbodySubscribe').append(show)
                                    }
                                }
                            })
                        } else {
                            alert("Password salah");
                        }
                    }
                }
            }
        })
    }  
};